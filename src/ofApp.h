#pragma once

#include "ofMain.h"

class MyClass{
public:
	void set(int _i, float _f){
		i = _i; f = _f;
	};
	int i;
	float f;
};

class ofApp : public ofBaseApp{
public:

	int screenWidth, screenHeight, colorDark, colorLight;
	float soundLevel, lightLevel, soundValue, lightValue;
	int baud = 9600;
	bool dark, soundTop, buttonLeft, buttonRight;

	char receivedData[20]; // length we receive
	char sendData = 1;
	ofSerial serial;

	ofSoundPlayer kick;
	ofSoundPlayer drop;
	ofSoundPlayer clap;
	ofSoundPlayer cymb;

	void setup();
	void update();
	void draw();
	void keyReleased(int key);
	void mousePressed(int x, int y, int button);
	void serialValues();
	void valuesConversion();
	void drawShape();
	void whichOne();
	void buttons();
};
